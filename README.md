# Eole robotframework tests automation  |  [06/2024]
Software test on eole3 software solution using robotframework and robotframework-browser

for package management we use in this project poetry and a pyproject.toml file

## Documentation 
For more information about the test naviguate to the [documentation](https://codimd.mim-libre.fr/_XEhBLmWRXq60fethjbirQ)
## Install poetry
Download and install poetry
```
curl -sSL https://install.python-poetry.org | python3 -
```
## Activate virtual environment
Activate the virtual environnement using poetry
```
$ poetry shell
```
## Install all dependencies
```
$ poetry install
```
## Initializise robotframework-browser library
```
$ rfbrowser init
```
## Run robot in a specific file
```
$ robot {PathToFile}/file.robot
```
## Run all robot tests of a folder (__init__.robot file required)
```
$ robot {PathToProject}/tests/LaBoite
# or run on current folder
$ robot .
```
## Deactivate virtual environment
```
$ deactivate

# OR

$ exit

```
