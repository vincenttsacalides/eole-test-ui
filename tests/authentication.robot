*** Settings ***
Library             Browser
Resource            ../resources/common.resource
Resource            ../resources/authentication.resource
Variables           ../data/common.yaml
Variables           ../data/authentication.yaml

Suite Setup         New Browser    browser=${browser}   headless=False
Test Setup          Run Keywords
...     Set Browser Timeout    30 seconds
...     AND         New Context    viewport={'width': 1600, 'height': 900} 
...     AND         Navigate to website    ${LaBoite_URL}
Test Teardown       Close Context
Suite Teardown      Close Browser

*** Test Cases ***
Register a new user
    Register new user       ${username_01}      ${mail_01}      ${password_01}

Connection with valid login
    Connect to the website      ${username_01}      ${password_01}

Connection with unknown username
    Connect to the website      ${bad_username}      ${password_01}

Connection with invalid password
    Connect to the website      ${username_01}   ${bad_password}

Disconnection
    Connect to the website      ${username_01}      ${password_01}
    Disconnect from LaBoite website
