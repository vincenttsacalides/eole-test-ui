*** Settings ***
Library             Browser
Resource            ../../resources/common.resource
Resource            ../../resources/authentication.resource
Resource            ../../resources/publications.resource
Variables           ../../data/common.yaml
Variables           ../../data/authentication.yaml

Suite Setup         Run Keywords
...     New Browser    browser=${browser}   headless=False
...     AND         New Context    viewport={'width': 1600, 'height': 900} 
...     AND         Navigate to website    ${LaBoite_URL}
...     AND         Connect to the website      ${username_01}      ${password_01}
Suite Teardown      Run Keywords
...     Disconnect from LaBoite website
...     AND         Close Context
...     AND         Close Browser

*** Test Cases ***
Create a new publications
    [Setup]    Navigate To Page    "Mes publications"
    [Documentation]    Test CREATE of CRUD
    KW Create Publication
    ...     Publication généré automatiquement par robotframework
    ...     Publication généré grâce au tests automatisé
    ...     Leeeeroy Jenkiinnsss !!!!

Search a specific publications
    [Documentation]    Test READ ONE of CRUD
    KW Search Publication    robotframework

Update a publication
    [Documentation]    Test UPDATE ONE of CRUD
    KW Update Publication

Delete a publication
    [Documentation]    Test DELETE of CRUD
    KW Delete Publication
