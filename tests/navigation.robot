*** Settings ***
Library             Browser
Resource            ../resources/common.resource
Resource            ../resources/authentication.resource
Variables           ../data/common.yaml
Variables           ../data/authentication.yaml

Suite Setup         New Browser    browser=${browser}   headless=False
Test Setup          Run Keywords
...     Set Browser Timeout    30 seconds
...     AND         New Context    viewport={'width': 1600, 'height': 900} 
...     AND         Navigate to website         ${LaBoite_URL}
...     AND         Connect to the website      ${username_01}      ${password_01}

Test Teardown       Run Keywords
...     Disconnect from LaBoite website
...     AND         Close Context
Suite Teardown      Close Browser

*** Test Cases ***
Navigate to all the navbar pages
    Click           span >> "Informations"
    Click           span >> "Mon Espace"
    Click           span >> "Mes publications"
    Click           span >> "Ma Structure"
    Click           span >> "Les services"
    Click           span >> "Les groupes"

Navigate to all the user dropdown pages
    Open the user dropdown
    Click           li >> "Mon profil"
    Open the user dropdown
    Click           li >> "Stockage de Médias"
    Open the user dropdown
    Click           li >> "Marque-pages"
    Open the user dropdown
    Click           li >> "Gestion des Groupes"
    Open the user dropdown
    Click           li >> "Contact"
    Open the user dropdown
    Click           li >> "Aide"
    Open the user dropdown
    ${about}    Get Element     li >> "À propos"
    Log     ${about}
    Click           li >> "À propos"