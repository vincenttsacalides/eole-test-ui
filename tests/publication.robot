*** Settings ***
Library             Browser
Resource            ../resources/common.resource
Resource            ../resources/authentication.resource
Resource            ../resources/publications.resource
Variables           ../data/common.yaml
Variables           ../data/authentication.yaml

Suite Setup         New Browser    browser=${browser}   headless=False
Test Setup          Run Keywords
...     Set Browser Timeout    30 seconds
...     AND         New Context    viewport={'width': 1600, 'height': 900} 
...     AND         Navigate to website    ${LaBoite_URL}
...     AND         Connect to the website      ${username_01}      ${password_01}
...     AND         Navigate To Page    "Mes publications"
Test Teardown       Run Keywords
...     Disconnect from LaBoite website
...     AND         Close Context
Suite Teardown      Close Browser

Set Browser Timeout    30s


*** Test Cases ***
Create a new publications
    [Documentation]    Test CREATE of CRUD
    KW Create Publication
    ...     Publication généré automatiquement par robotframework
    ...     Publication généré grâce au tests automatisé
    ...     Leeeeroy Jenkiinnsss !!!!

Search a specific publications
    [Documentation]    Test READ ONE of CRUD
    KW Search Publication    robotframework

Update a publication
    [Documentation]    Test UPDATE ONE of CRUD
    KW Update Publication

Delete a publication
    [Documentation]    Test DELETE of CRUD
    KW Delete Publication





