*** Settings ***
Library             Browser
Resource            ../resources/common.resource
Resource            ../resources/authentication.resource
Resource            ../resources/publications.resource
Variables           ../data/common.yaml
Variables           ../data/authentication.yaml

Suite Setup         New Browser    browser=${browser}   headless=False
Test Setup          Run Keywords
...     New Context    viewport={'width': 1600, 'height': 900}
...     AND         Navigate to website    ${URL of application}                            # TO FILL
...     AND         Connect to the website      ${username to use}      ${password to use}  # TO FILL
...     AND         Navigate To Page    "The page name string"                              # TO FILL
Test Teardown       Run Keywords
...     Disconnect from LaBoite website                                                     # TO FILL
...     AND         Close Context
Suite Teardown      Close Browser

*** Test Cases ***
My Test                                                                                     # TO FILL
    [Documentation]    Documentation of My test                                             # TO FILL
    KW Resources Keyword of My Test (Controller)                                            # TO FILL
