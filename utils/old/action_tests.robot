*** Settings ***
Library             Browser
Resource            ../resources/common.resource
Resource            ../resources/authentication.resource
Variables           ../data/common.yaml
Variables           ../data/authentication.yaml

Suite Setup         New Browser    browser=${browser}   headless=False
Test Setup          Run Keywords    
...     New Context
...     AND     Navigate to page            ${LaBoite_URL}
...     AND     Connect to the website      ${username_01}      ${password_01}

Test Teardown       Run Keywords
...     Disconnect from LaBoite website
...     AND Close Context
Suite Teardown      Close Browser

*** Test Cases ***
See All Notification And Delete It
    Click   id=NotificationsBell
    Sleep   1500ms
    ${delete_notification}          Run Keyword And Return Status      Click   "Supprimer toutes les notifications"
    Run Keyword If      ${delete_notification} == True       Click   "Supprimer toutes les notifications"
    Sleep   1s

Toggle dark and light theme
    Click   svg[data-testid="DarkModeOutlinedIcon"]
    Sleep   2s

    Click   svg[data-testid="LightModeOutlinedIcon"]
    Sleep   1s
