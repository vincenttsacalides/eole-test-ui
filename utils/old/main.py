#!/usr/bin/env python3
from robot import run_cli
from argparse import ArgumentParser

parser = ArgumentParser()

parser.add_argument('-a', '--all', action='store_true')
parser.add_argument('-s','--specific', choices=['publications', 'structure', 'services'])

args = parser.parse_args()

def run_all_tests():
    run_cli(["-d","logs","tests"])

def run_publication_tests()
    run_cli(["-d","logs","tests/publications.robot"])

def run_structure_tests()
    run_cli(["-d","logs","tests/structure.robot"])

def run_services_tests()
    run_cli(["-d","logs","tests/services.robot"])

